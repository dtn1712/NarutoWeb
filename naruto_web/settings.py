# coding=utf-8
"""Common settings and globals."""
from __future__ import absolute_import

import os
from datetime import timedelta
from os import environ
from sys import path

ROOT_PATH = os.path.abspath(__file__ + "/../../")
PROJECT_PATH = os.path.dirname(__file__)
path.append(ROOT_PATH)
path.append(PROJECT_PATH)

PROJECT_NAME = os.path.basename(ROOT_PATH).replace("-", "_")
SITE_NAME = "The Last Legend"

DEBUG = False

########## MANAGER CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = (
    ('Dang Nguyen', 'dtn1712@gmail.com'),
    ('Hoa Tran', 'tranhoaac@gmail.com'),
    ('Dang Nguyen', 'dangnguyen_1712@yahoo.com'),
    ('Sakura Mobile', 'teamsakuramobile@gmail.com')
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS
########## END MANAGER CONFIGURATION

########## GENERAL CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#time-zone
TIME_ZONE = 'America/Los_Angeles'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = 'en-us'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True
########## END GENERAL CONFIGURATION


########## MEDIA CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = os.path.join(PROJECT_PATH, 'assets/media')

# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = '/media/'
########## END MEDIA CONFIGURATION


########## STATIC FILE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = 'staticfiles'

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = (
    os.path.join(PROJECT_PATH, 'assets/static'),
)

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)
########## END STATIC FILE CONFIGURATION


########## SECRET CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
SECRET_KEY = "chbrc3p7q%g9e80a(&bm$ci6ygdc_ak99q6ep90!#evq7yc@@@"

# See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-FIXTURE_DIRS
FIXTURE_DIRS = (
    os.path.join(PROJECT_PATH, 'db/fixtures'),
)
########## END FIXTURE CONFIGURATION

DEFAULT_TEMPLATE_DIRS = [
    os.path.join(PROJECT_PATH, 'assets/templates'),
    os.path.join(PROJECT_PATH, 'assets/templates/sites'),
    os.path.join(PROJECT_PATH, 'assets/templates/sites/apps'),
    os.path.join(PROJECT_PATH, 'assets/templates/sites/apps/auth'),
]

DEFAULT_TEMPLATE_CONTEXT_PROCESSOR = [
    'django.template.context_processors.debug',
    'django.template.context_processors.request',
    'django.template.context_processors.static',
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
    'django_mobile.context_processors.flavour',
    "naruto_web.apps.main.context_processors.site_data",
    "naruto_web.apps.main.context_processors.global_data",
]

DEFAULT_TEMPLATE_EXTENSIONS = [
    "jinja2.ext.do",
    "jinja2.ext.loopcontrols",
    "jinja2.ext.with_",
    "jinja2.ext.i18n",
    "jinja2.ext.autoescape",
    "django_jinja.builtins.extensions.CsrfExtension",
    "django_jinja.builtins.extensions.CacheExtension",
    "django_jinja.builtins.extensions.TimezoneExtension",
    "django_jinja.builtins.extensions.UrlsExtension",
    "django_jinja.builtins.extensions.StaticFilesExtension",
    "django_jinja.builtins.extensions.DjangoFiltersExtension",
]

DEFAULT_TEMPLATE_FILTERS = {
    "get_dictionary_item_value": "naruto_web.apps.main.templatetags.filters.get_dictionary_item_value",
    "display_elapse_time": "naruto_web.apps.main.templatetags.filters.display_elapse_time",
    "words_first_char_upper": "naruto_web.apps.main.templatetags.filters.words_first_char_upper",
    "first_word_only": "naruto_web.apps.main.templatetags.filters.first_word_only",
    "first_char_only": "naruto_web.apps.main.templatetags.filters.first_char_only",
    "initial": "naruto_web.apps.main.templatetags.filters.initial",
}

DEFAULT_TEMPLATE_GLOBALS = {
    "load_css": "naruto_web.apps.main.templatetags.static_loader.load_css",
    "load_js": "naruto_web.apps.main.templatetags.static_loader.load_js",
    "get_image_url": "naruto_web.apps.main.templatetags.global_functions.get_image_url",
    "get_image_element": "naruto_web.apps.main.templatetags.global_functions.get_image_element",
    "shorten_content": "naruto_web.apps.main.templatetags.global_functions.shorten_content",
}

DEFAULT_TEMPLATE_FILE_EXTENSION = ".jinja.html"

TEMPLATE_LOADERS = [
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader'
]

TEMPLATES = [
    {
        "NAME": "jinja",
        "BACKEND": "django_jinja.backend.Jinja2",
        'DIRS': DEFAULT_TEMPLATE_DIRS,
        "OPTIONS": {
            "match_extension": DEFAULT_TEMPLATE_FILE_EXTENSION,
            "match_regex": r"^(?!admin/|!debug_toolbar/).*",
            "newstyle_gettext": True,
            'context_processors': DEFAULT_TEMPLATE_CONTEXT_PROCESSOR,
            "filters": DEFAULT_TEMPLATE_FILTERS,
            "globals": DEFAULT_TEMPLATE_GLOBALS,
            "extensions": DEFAULT_TEMPLATE_EXTENSIONS,
            "bytecode_cache": {
                "name": "default",
                "backend": "django_jinja.cache.BytecodeCache",
                "enabled": False,
            },
            "autoescape": False,
            "translation_engine": "django.utils.translation",
        }
    },
    {
        "NAME": "django",
        "BACKEND": 'django.template.backends.django.DjangoTemplates',
        'DIRS': DEFAULT_TEMPLATE_DIRS,
        'OPTIONS': {
            'context_processors': DEFAULT_TEMPLATE_CONTEXT_PROCESSOR,
        },
    },
]

ROOT_URLCONF = '%s.urls' % PROJECT_NAME

DJANGO_APPS = (
    # Default Django apps:
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Useful template tags:
    'django.contrib.humanize',

    # Admin panel and documentation:
    'django.contrib.admin',
    # 'django.contrib.admindocs',
)

THIRD_PARTY_APPS = (
    'defender',
    "django_jinja",
    'crispy_forms',
    "timelog",
    "cloudinary",
)

LOCAL_APPS = (
    "naruto_web.apps.about",
    "naruto_web.apps.account",
    "naruto_web.apps.ajax",
    "naruto_web.apps.main",
    "naruto_web.apps.photo",
    "naruto_web.apps.payment",
)

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

TIMELOG_LOG = os.path.join(ROOT_PATH, "logs", "timelog.log")

SITE_NAME_LOG = SITE_NAME.replace(" ", "_").lower()

SITE_LOG_HANDLER = "logfile_" + SITE_NAME_LOG

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
        'plain': {
            'format': '%(asctime)s %(message)s'
        },
    },
    'filters': {
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'filters': ['require_debug_true'],
            'formatter': 'simple'
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler',
            'formatter': 'verbose'
        },
        SITE_LOG_HANDLER: {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(ROOT_PATH, "logs", SITE_NAME_LOG + ".log"),
            'maxBytes': 1024 * 1024 * 5,  # 5 MB
            'backupCount': 5,
            'formatter': 'verbose'
        },
        'logfile_request': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(ROOT_PATH, "logs/request.log"),
            'maxBytes': 1024 * 1024 * 5,  # 5 MB
            'backupCount': 5,
            'formatter': 'verbose'
        },
        'timelog': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': TIMELOG_LOG,
            'maxBytes': 1024 * 1024 * 5,  # 5 MB
            'backupCount': 5,
            'formatter': 'plain',
        },

    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
        },
        'django.request': {
            'handlers': ['logfile_request'],
            'level': 'ERROR',
            'propagate': False,
        },
        SITE_NAME_LOG: {
            'handlers': [SITE_LOG_HANDLER],
            'level': 'DEBUG',
            'propogate': True,
        },
        'timelog.middleware': {
            'handlers': ['timelog'],
            'level': 'DEBUG',
            'propogate': False,
        }
    },
}

CACHE_OBJECT_LIST_TIMEOUT = 60 * 10


########## WSGI CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'wsgi.application'
########## END WSGI CONFIGURATION


AUTH_PROFILE_MODULE = 'main.UserProfile'

SERIALIZATION_MODULES = {
    'json': "django.core.serializers.json",
}

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'naruto_web.apps.account.backends.AuthenticationBackend'
)

ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_PASSWORD_MIN_LENGTH = 6
ACCOUNT_EMAIL_VERIFICATION = "none"
ACCOUNT_LOGOUT_ON_GET = True

LOGIN_URL = '/account/login'
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'

ACCOUNT_USERNAME_BLACKLIST = [
    'admin', 'signup', 'login', 'password', "accounts",
    'logout', 'confirm_email', 'search', 'settings',
    'buzz', 'messages', "about", 'api', 'asset', 'photo',
    'feeds', 'friends'
]

OW_LY_API_KEY = "6sv891CJpDcuiz8eyRHfy"

CRISPY_TEMPLATE_PACK = 'bootstrap3'

KEY_PREFIX = SITE_NAME

TIMELOG_IGNORE_URIS = (
    '^/admin/',  # Ignores all URIs beginning with '/admin/'
    '.jpg$',  # Ignores all URIs ending in .jpg
    '.js$',  # Ignores all URIs ending in .js
    '.css$',  # Ignores all URIs ending in .css
)

PHOTO_ALIASES = {
    'default': {
        'width': '200',
    },
    "post_detail_small": {
        "width": "50",
        "height": "50",
        "crop": "center"
    },
    "post_detail_medium": {
        "height": "350",
        "crop": "center"
    },
    "post_detail_large": {
        "width": "600",
        "height": "400",
        "crop": "center"
    },
    "search_default": {
        "width": "80",
        "height": "80",
    }
}

DEFAULT_APP_NAME = "main"

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

import cloudinary

cloudinary.config(
    cloud_name="dlnrgtqqv",
    api_key="552849736355724",
    api_secret="j3lS-0qhDKK-u-ozeJg2D3X9kNI",
    cdn_subdomain=True,
    secure=True
)

MAX_IMAGE_WIDTH = 900
MAX_IMAGE_HEIGHT = 600
DEFAULT_IMAGE_WIDTH = 600
DEFAULT_IMAGE_HEIGHT = 400

UPLOAD_FOLDER = "upload"


FANPAGE_URL = "https://www.facebook.com/huyenthoaicuoicung/"

SHOW_DOWNLOAD = False



# coding=utf-8
STAGE = "dev"

DEBUG = True

THUMBNAIL_DEBUG = True

WEBSITE_URL = "http://localhost:8000"

API_URL = "http://localhost:1931"

ADMIN_URL = "http://localhost:1930"

DATABASES = {
    "default": {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': "naruto_web",
        'USER': "root",
        'PASSWORD': "123456",
        'HOST': "localhost",
        'PORT': "3306",
        'CONN_MAX_AGE': 600,
    }
}

########## CACHE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "SOCKET_TIMEOUT": 5,
        }
    }
}
########## END CACHE CONFIGURATION


########## CELERY CONFIGURATION
# See: http://docs.celeryq.org/en/latest/configuration.html#celery-always-eager
# CELERY_ALWAYS_EAGER = True

# See: http://docs.celeryproject.org/en/latest/configuration.html#celery-eager-propagates-exceptions
# CELERY_EAGER_PROPAGATES_EXCEPTIONS = True
########## END CELERY CONFIGURATION


########## TOOLBAR CONFIGURATION
# See: https://github.com/django-debug-toolbar/django-debug-toolbar#installation
INSTALLED_APPS += (
    # 'debug_toolbar',
    # 'template_timings_panel',
    # "template_profiler_panel",
    # "haystack_panel",
    # 'profiler',
)

# See: https://github.com/django-debug-toolbar/django-debug-toolbar#installation
INTERNAL_IPS = ('127.0.0.1',)


DEBUG_TOOLBAR_CONFIG = {
    'JQUERY_URL': '',
}


DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel'
]

########## END TOOLBAR CONFIGURATION

STATIC_URL = '/static/'

MIDDLEWARE_CLASSES = (
    # 'timelog.middleware.TimeLogMiddleware',
    # 'debug_toolbar.middleware.DebugToolbarMiddleware',

    'django.middleware.security.SecurityMiddleware',
    # 'django.middleware.cache.UpdateCacheMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',

    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'defender.middleware.FailedLoginMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',

    # 'django.middleware.cache.FetchFromCacheMiddleware',
)

BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'

CACHE_MIDDLEWARE_SECONDS = 0

CSS_STATIC_URL_PATH_REPLACEMENT = {

}

STRIPE_API_PUBLIC_KEY='pk_test_bTcksnbJndFmQ90hjiFoSLtC'
STRIPE_API_SECRET_KEY='sk_test_Pr3Oo76R2AmO2ay6knxctkLB'

SERVER_IP = [
    {
        "name": "Làng lá",
        "ip": "127.0.0.1",
        "tcpPort": "19158",
        "udpPort": "19159"
    }
]


SHOW_DOWNLOAD = True

BUILD_VERSION_ID = 'H8G22b9QxfzoZ8RA5W57wM'
