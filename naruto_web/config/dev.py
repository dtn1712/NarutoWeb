# coding=utf-8
STAGE = "dev"

DEBUG = True

THUMBNAIL_DEBUG = True

WEBSITE_URL = "http://localhost:8000"

API_URL = "http://localhost:1931"

ADMIN_URL = "http://localhost:1930"

DATABASES = {
    "default": {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': "naruto_web",
        'USER': "root",
        'PASSWORD': "123456",
        'HOST': "localhost",
        'PORT': "3306",
        'CONN_MAX_AGE': 600,
    }
}

########## CACHE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "SOCKET_TIMEOUT": 5,
        }
    }
}
########## END CACHE CONFIGURATION


########## CELERY CONFIGURATION
# See: http://docs.celeryq.org/en/latest/configuration.html#celery-always-eager
# CELERY_ALWAYS_EAGER = True

# See: http://docs.celeryproject.org/en/latest/configuration.html#celery-eager-propagates-exceptions
# CELERY_EAGER_PROPAGATES_EXCEPTIONS = True
########## END CELERY CONFIGURATION


########## TOOLBAR CONFIGURATION
# See: https://github.com/django-debug-toolbar/django-debug-toolbar#installation
INSTALLED_APPS += (
    # 'debug_toolbar',
    # 'template_timings_panel',
    # "template_profiler_panel",
    # "haystack_panel",
    # 'profiler',
)

# See: https://github.com/django-debug-toolbar/django-debug-toolbar#installation
INTERNAL_IPS = ('127.0.0.1',)


DEBUG_TOOLBAR_CONFIG = {
    'JQUERY_URL': '',
}


DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel'
]

########## END TOOLBAR CONFIGURATION

STATIC_URL = '/static/'

MIDDLEWARE_CLASSES = (
    # 'timelog.middleware.TimeLogMiddleware',
    # 'debug_toolbar.middleware.DebugToolbarMiddleware',

    'django.middleware.security.SecurityMiddleware',
    # 'django.middleware.cache.UpdateCacheMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',

    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'defender.middleware.FailedLoginMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',

    # 'django.middleware.cache.FetchFromCacheMiddleware',
)

BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'

CACHE_MIDDLEWARE_SECONDS = 0

CSS_STATIC_URL_PATH_REPLACEMENT = {

}

STRIPE_API_PUBLIC_KEY='pk_test_bTcksnbJndFmQ90hjiFoSLtC'
STRIPE_API_SECRET_KEY='sk_test_Pr3Oo76R2AmO2ay6knxctkLB'

SERVER_IP = [
    {
        "name": "Làng lá",
        "ip": "127.0.0.1",
        "tcpPort": "19158",
        "udpPort": "19159"
    }
]


SHOW_DOWNLOAD = True
