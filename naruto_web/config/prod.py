# coding=utf-8
"""Production settings and globals."""

from os import environ

STAGE = "prod"

WEBSITE_URL = "http://huyenthoaicuoicung.com"

API_URL = "http://128.199.235.198:1931"

ADMIN_URL = "http://128.199.235.198:1930"

MIDDLEWARE_CLASSES = (
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'defender.middleware.FailedLoginMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': environ.get("DATABASE_NAME"),
        'USER': environ.get("DATABASE_USER"),
        'PASSWORD': environ.get("DATABASE_PASSWORD"),
        'HOST': environ.get("DATABASE_HOST"),
        'PORT': environ.get("DATABASE_PORT"),
        'CONN_MAX_AGE': 600,
    }
}

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": environ.get("CACHE_LOCATION", 'redis://127.0.0.1:6379/1'),
        'TIMEOUT': 60,
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "SOCKET_TIMEOUT": 5,
        }
    }
}


########## STORAGE CONFIGURATION
# See: http://django-storages.readthedocs.org/en/latest/index.html
INSTALLED_APPS += (
    'storages',
)

# See: http://django-storages.readthedocs.org/en/latest/backends/amazon-S3.html#settings
STATICFILES_STORAGE = DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'

# See: http://django-storages.readthedocs.org/en/latest/backends/amazon-S3.html#settings
AWS_IS_GZIPPED = True

# See: http://django-storages.readthedocs.org/en/latest/backends/amazon-S3.html#settings
AWS_AUTO_CREATE_BUCKET = True
AWS_QUERYSTRING_AUTH = False

# See: http://django-storages.readthedocs.org/en/latest/backends/amazon-S3.html#settings
AWS_ACCESS_KEY_ID = environ.get("AWS_ACCESS_KEY_ID")
AWS_SECRET_ACCESS_KEY = environ.get("AWS_SECRET_ACCESS_KEY")
AWS_STORAGE_BUCKET_NAME = environ.get("AWS_STORAGE_BUCKET_NAME")

# AWS cache settings, don't change unless you know what you're doing:
AWS_EXPIRY = 60 * 60 * 24 * 7
AWS_HEADERS = {
    'Cache-Control': 'max-age=%d, s-maxage=%d, must-revalidate' % (AWS_EXPIRY,
                                                                   AWS_EXPIRY)
}

STATIC_URL = environ.get("STATIC_URL")

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

ALLOWED_HOSTS = ['*']


CACHE_MIDDLEWARE_SECONDS = 0

CSS_STATIC_URL_PATH_REPLACEMENT = {
    "https://d3209dole6nx2c.cloudfront.net/": STATIC_URL,
    "../resources/": STATIC_URL + "css/prod/"
}

STRIPE_API_PUBLIC_KEY='pk_live_XN5HcWWzR073ybMr6EVLskDK'
STRIPE_API_SECRET_KEY='sk_live_wnvVkoqeIzx7Bjw7RZE7VVaS'

SERVER_IP = [
    {
        "name": "Làng lá",
        "ip": "128.199.235.198",
        "tcpPort": "19158",
        "udpPort": "19159"
    }
]

SHOW_DOWNLOAD = True
