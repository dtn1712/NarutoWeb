from django.conf.urls import *

from naruto_web.apps.ajax import views

urlpatterns = [
    url(r"^payment/exchange_rate", views.get_exchange_rate, name="list_payment_method"),
    url(r"^admin/reload_game", views.reload_game, name="admin_reload_game"),
]
