# coding=utf-8
import logging

import requests
from django.http import JsonResponse

from naruto_web.apps.ajax.helper import validate_request, get_request_params
from naruto_web.apps.app_helper import generate_html_snippet, get_user_login_object
from naruto_web.settings import API_URL, ADMIN_URL

logger = logging.getLogger(__name__)


def get_exchange_rate(request):
    result = {}
    try:
        validate_request(request, ['GET'])
        data = get_request_params(request, ['payment_method'])
        url = API_URL + "/api/v1/payment/external/exchange-rates?paymentMethodKey=" + data['payment_method']

        r = requests.get(url)
        status_code = r.status_code
        response = r.json()

        if status_code == 200:
            if response['success']:
                result['success'] = True

                request.session['exchange_rates'] = response['exchangeRates']
                result['message_snippet'] = generate_html_snippet(request, "exchange_rate_list",
                                                                  {"exchange_rates": response['exchangeRates'],
                                                                   "payment_method": response['paymentMethod']})
            else:
                result['success'] = False
                result["error_message"] = response['errorMessage']
        else:
            logger.exception(response['error'])
            result["error_message"] = "Có lỗi xảy ra. Vui lòng thử lại"
    except Exception as e:
        logger.exception(e)
        result['success'] = False
        result['error_message'] = "Có lỗi xảy ra. Vui lòng thử lại"
    return JsonResponse(result)


def reload_game(request):
    result = {}
    try:
        user_login = get_user_login_object(request)

        validate_request(request, ['POST'])
        url = ADMIN_URL + "/admin/game/reload"
        headers = {'Authorization': str(user_login['account_id']) + ":" + str(user_login['api_key'])}

        r = requests.post(url, headers=headers)
        status_code = r.status_code

        if status_code == 200:
            result['success'] = True
        else:
            result["error_message"] = "Có lỗi xảy ra. Vui lòng thử lại"
    except Exception as e:
        logger.exception(e)
        result['success'] = False
        result['error_message'] = "Có lỗi xảy ra. Vui lòng thử lại"
    return JsonResponse(result)
