from django.conf.urls import *

from naruto_web.apps.photo.views import UploadPhotoView

urlpatterns = [
    url(r"^upload/$", UploadPhotoView.as_view(), name="upload_photo"),
]
