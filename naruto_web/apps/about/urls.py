from django.conf.urls import *

from naruto_web.apps.about.views import TermView

urlpatterns = [
    url(r"^term$", TermView.as_view(), name="term"),
]
