import logging

from django.views.generic import TemplateView

from naruto_web.apps.app_views import AppBaseView

logger = logging.getLogger(__name__)

APP_NAME = "about"


class TermView(AppBaseView, TemplateView):
    app_name = APP_NAME
    template_name = "term"
