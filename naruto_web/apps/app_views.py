import logging
from django.core.exceptions import NON_FIELD_ERRORS
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect

from django.views.generic import FormView
from django.views.generic.base import TemplateResponseMixin, ContextMixin

from naruto_web.apps.app_helper import capitalize_first_letter, get_user_login_object
from naruto_web.apps.app_helper import get_base_template_path
from naruto_web.apps.app_helper import get_template_path, handle_request_get_message
from naruto_web.apps.main.models import Config
from naruto_web.settings import SITE_NAME, DEFAULT_TEMPLATE_FILE_EXTENSION


logger = logging.getLogger(__name__)


def is_authenticated(request):
    user_login = get_user_login_object(request)
    return user_login is not None and int(user_login['account_id']) >= 0


def set_context_data(context, key):
    try:
        context[key] = Config.objects.get(key=key).value
    except:
        context[key] = ''


class AppBaseView(TemplateResponseMixin, ContextMixin):
    sub_path = "/page/"
    template_extension = DEFAULT_TEMPLATE_FILE_EXTENSION

    def __init__(self):
        self.user_login = None

    def dispatch(self, request, *args, **kwargs):
        if "session_id" in request.GET and request.GET['session_id'] != request.session.session_key:
            return HttpResponseRedirect("/")

        self.user_login = get_user_login_object(request)
        return super(AppBaseView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AppBaseView, self).get_context_data(**kwargs)
        context['user_login'] = self.user_login
        context['app_name'] = self.app_name
        context['page_type'] = self.app_name + "_" + self.template_name

        set_context_data(context, 'APK_DOWNLOAD_LINK')
        set_context_data(context, 'APK_LOW_DOWNLOAD_LINK')
        set_context_data(context, 'CH_PLAY_DOWNLOAD_LINK')
        set_context_data(context, 'WINDOW_DOWNLOAD_LINK')
        set_context_data(context, 'IOS_DOWNLOAD_LINK')

        data = {
            'user_login': self.user_login,
            'site_name': capitalize_first_letter(SITE_NAME)
        }

        context['request_message'] = handle_request_get_message(self.request, data)
        context['base_template'] = get_base_template_path(None, self.template_extension)
        context['app_base_template'] = get_base_template_path(self.app_name, self.template_extension)

        return context

    def get_template_names(self):
        return [get_template_path(self.app_name, self.template_name, self.sub_path, self.template_extension)]


class AuthenticatedView(AppBaseView):

    def dispatch(self, request, *args, **kwargs):
        if not is_authenticated(request):
            return HttpResponseRedirect(reverse_lazy("login") + "?next=" + request.path)
        return super(AuthenticatedView, self).dispatch(request, *args, **kwargs)


class AppFormView(AppBaseView, FormView):

    def post(self, request, *args, **kwargs):
        """
        Handles POST requests, instantiating a form instance with the passed
        POST variables and then checked for validity.
        """
        form = self.get_form()
        if form.is_valid():
            try:
                success_url = self.get_success_url()
                return form.save(request, redirect_url=success_url)
            except Exception as e:
                form.add_error(NON_FIELD_ERRORS, e)

        return self.form_invalid(form)
