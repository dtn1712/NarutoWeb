from naruto_web.settings import SITE_NAME, WEBSITE_URL, \
    STRIPE_API_PUBLIC_KEY, FANPAGE_URL, SHOW_DOWNLOAD, MOMO_PHONE_NUMBER

SECONDS_PER_DAY = 86400
SECONDS_PER_HOUR = 3600
SECONDS_PER_MINUTE = 60

SITE_DATA = {
    "SITE_NAME": SITE_NAME,
    "SITE_TITLE": SITE_NAME,
    "SITE_DESCRIPTION": SITE_NAME,
    "SITE_KEYWORDS": "Pass, course, webinar talk, tech talk, tech, learning",
    "SITE_URL": WEBSITE_URL,
    "STRIPE_API_PUBLIC_KEY": STRIPE_API_PUBLIC_KEY,
    "FANPAGE_URL": FANPAGE_URL,
    "SHOW_DOWNLOAD": SHOW_DOWNLOAD,
    "MOMO_PHONE_NUMBER": MOMO_PHONE_NUMBER,
}

KEYWORDS_URL = [
    'admin', 'signup', 'login', 'password', "accounts",
    'logout', 'confirm_email', 'search', 'settings',
    'buzz', 'messages', "about", 'api', 'asset', 'photo',
    'feeds', 'friends'
]

MESSAGE_SNIPPET_TEMPLATE = {
    "action_error": "messages/apps/action_error.html",
    "action_success": "messages/apps/action_success.html",
}

HTML_SNIPPET_TEMPLATE = {
    "exchange_rate_list": "sites/snippet/exchange_rate_list.jinja.html",
}
