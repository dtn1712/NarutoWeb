from django.conf.urls import *

from naruto_web.apps.payment.views import PaymentChargeView

urlpatterns = [
    url(r"^charge", PaymentChargeView.as_view(), name="payment_charge"),
]
