# coding=utf-8
import logging

import requests
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView

from naruto_web.apps.app_helper import get_user_login_object, generate_message
from naruto_web.apps.app_views import AuthenticatedView
from naruto_web.settings import API_URL

logger = logging.getLogger(__name__)

APP_NAME = "payment"


def check_amount_valid(amount_data, exchange_rates):
    try:
        amount = amount_data.split("-")[0]
        currency = amount_data.split("-")[1]

        for exchange_rate in exchange_rates:
            if str(exchange_rate['realMoneyAmount']) == amount and str(exchange_rate['realMoneyUnit']) == currency:
                return True
    except Exception as e:
        logger.exception(e)

    return False


class PaymentChargeView(AuthenticatedView, TemplateView):
    app_name = APP_NAME
    template_name = "charge"

    def get_context_data(self, **kwargs):
        context = super(PaymentChargeView, self).get_context_data(**kwargs)
        url = API_URL + "/api/v1/payment/external/payment-methods"

        r = requests.get(url)
        status_code = r.status_code
        response = r.json()

        if status_code == 200:
            if response['success']:
                context["payment_methods"] = response['paymentMethods']
            else:
                context["request_message"] = generate_message("action_error", {"message": response['errorMessage']})
        else:
            logger.exception("Failed to reset password. Exception:" + response['error'])
            context["request_message"] = generate_message("action_error", {"message": "Có lỗi xảy ra. Vui lòng thử lại"})

        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        user_login = get_user_login_object(request)

        if request.POST['payment_method'] is None or len(request.POST['payment_method']) == 0:
            context['errors'] = [u"Vui lòng chọn phương thức thanh toán"]
        else:

            if request.POST['payment_method'].split("-")[1] == "MOBILE_CARD":
                body = {
                    "data": {
                        "pin": request.POST['pin'],
                        "serial": request.POST['serial']
                    }
                }
            else:
                if not check_amount_valid(request.POST['amount'], request.session.get('exchange_rates', [])):
                    context["errors"] = [u"Mệnh giá không hợp lệ"]
                elif request.POST['payment_method'].split("-")[1] == "CREDIT_CARD":
                    body = {
                        "data": {
                            "token": request.POST['stripeToken'],
                            "amount": request.POST['amount'].split("-")[0],
                            "currency": request.POST['amount'].split("-")[1],
                        }
                    }
                else:
                    body = {
                        "data": {
                            "amount": request.POST['amount'].split("-")[0],
                            "currency": request.POST['amount'].split("-")[1]
                        }
                    }

            headers = {'Authorization': str(user_login['account_id']) + ":" + str(user_login['api_key'])}

            url = API_URL + "/api/v1/payment/external/make-payment?paymentMethod=" + \
                  request.POST['payment_method'].split("-")[0]

            r = requests.post(url, json=body, headers=headers)
            status_code = r.status_code
            response = r.json()

            if status_code == 200:
                if response['success']:
                    message_data = {"message": u"Bạn đã nạp tiền thành công"}
                    if "trackingCode" in response['paymentResultInfo'] and "email" in response['paymentResultInfo']:
                        message_data = {"message": u"Tracking code đã được gửi đến email " + response['paymentResultInfo']['email'] +
                                                   u". Xin nhập code khi chuyển tiền" }

                    request.session['message_data'] = message_data
                    return HttpResponseRedirect("/?message_type=action_success")
                else:
                    context["errors"] = [unicode(response['errorMessage'])]
            else:
                logger.exception("Failed to charge. Exception:" + response['error'])
                context["request_message"] = [u"Có lỗi xảy ra. Vui lòng thử lại"]

        return self.render_to_response(context)
