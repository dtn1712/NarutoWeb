from django.conf.urls import *
from django.views.generic import RedirectView

from naruto_web.apps.account.views import LoginView, LogoutView, PasswordResetView, \
    PasswordResetDoneView, PasswordChangeView, PasswordResetFromKeyView, \
    DashboardPlayerView, DashboardMerchantView, DashboardAdminView

urlpatterns = [

    url(r"^login$", LoginView.as_view(), name="login"),
    url(r"^login/$", RedirectView.as_view(url='/account/login', permanent=False)),

    url(r"^logout$", LogoutView.as_view(), name="logout"),
    url(r"^logout/$", RedirectView.as_view(url='/account/logout', permanent=False)),

    url(r"^dashboard/admin", DashboardAdminView.as_view(), name="dashboard_admin"),
    url(r"^dashboard/merchant$", DashboardMerchantView.as_view(), name="dashboard_merchant"),
    url(r"^dashboard$", DashboardPlayerView.as_view(), name="dashboard_player"),

    url(r"^password/change$", PasswordChangeView.as_view(), name="change_password"),

    url(r"^password/reset$", PasswordResetView.as_view(), name="reset_password"),
    url(r"^password/reset/$", RedirectView.as_view(url='/account/password/reset', permanent=False)),
    url(r"^password/reset/done$", PasswordResetDoneView.as_view(), name="reset_password_done"),
    url(r"^password/reset/key/(?P<key>[-:\w]+)/$", PasswordResetFromKeyView.as_view(),
        name="reset_password_from_key"),

]
