# coding=utf-8
import logging

import requests
from django.core.exceptions import NON_FIELD_ERRORS
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.views.generic import FormView
from django.views.generic import TemplateView

from naruto_web.apps.account.forms import AccountForm, ResetPasswordForm, ChangePasswordForm, ResetPasswordKeyForm, \
    RequestPaymentCodeForm
from naruto_web.apps.app_helper import get_next_redirect_url, get_request_param
from naruto_web.apps.app_views import AppBaseView, AppFormView, is_authenticated, AuthenticatedView
from naruto_web.apps.main.models import Config
from naruto_web.settings import API_URL, ADMIN_URL

logger = logging.getLogger(__name__)

APP_NAME = "account"

MERCHANDISE = "MERCHANDISE"
ADMIN = "ADMIN"


class LoginView(AppFormView):
    app_name = APP_NAME
    template_name = "login"
    form_class = AccountForm
    success_url = "/account/dashboard"
    redirect_field_name = "next"

    def dispatch(self, request, *args, **kwargs):
        if is_authenticated(request):
            return HttpResponseRedirect("/account/dashboard")
        return super(LoginView, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        # Explicitly passed ?next= URL takes precedence=
        ret = (get_next_redirect_url(self.request, self.redirect_field_name) or self.success_url)
        return ret

    def get_context_data(self, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        context['next'] = self.request.GET.get(self.redirect_field_name, "")
        if "form" not in context:
            context['form'] = AccountForm()
        return context


class LogoutView(AppBaseView, FormView):
    app_name = APP_NAME
    template_name = "logout"

    def get(self, request, *args, **kwargs):
        if "user" in request.session:
            del request.session['user']
        request.session.flush()
        return HttpResponseRedirect("/")


class PasswordResetView(FormView, AppBaseView):
    app_name = APP_NAME
    template_name = "password_reset"
    form_class = ResetPasswordForm

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            try:
                email = form.save()
                self.success_url = reverse_lazy("reset_password_done") + "?email=" + email
                return HttpResponseRedirect(self.get_success_url())
            except Exception as e:
                form.add_error(NON_FIELD_ERRORS, e)

        return self.form_invalid(form)


class PasswordResetDoneView(AppBaseView, TemplateView):
    app_name = APP_NAME
    template_name = "password_reset_done"

    def get_context_data(self, **kwargs):
        context = super(PasswordResetDoneView, self).get_context_data(**kwargs)
        email = get_request_param(self.request, "email", None)
        context['email'] = email
        return context


class PasswordChangeView(AuthenticatedView, AppFormView):
    app_name = APP_NAME
    template_name = "password_change"
    form_class = ChangePasswordForm
    success_url = "/?message_type=action_success"


def check_confirmation_code(key):
    if key is None or len(key) == 0:
        return -1

    url = API_URL + "/api/v1/account/password/reset/confirmation/" + key

    r = requests.get(url)
    status_code = r.status_code
    response = r.json()

    if status_code == 200:
        if response['success']:
            return response['accountId']
    else:
        logger.exception("Failed to check password reset confirmation code. Exception:" + response['error'])

    return -1


class PasswordResetFromKeyView(AppBaseView, FormView):
    app_name = APP_NAME
    template_name = "password_reset_from_key"
    form_class = ResetPasswordKeyForm
    success_url = "/account/login?message_type=action_success"

    def __init__(self):
        super(PasswordResetFromKeyView, self).__init__()
        self.key = None
        self.account_id = -1

    def dispatch(self, request, *args, **kwargs):
        self.key = kwargs['key']
        self.account_id = check_confirmation_code(kwargs['key'])
        if self.account_id == -1:
            return HttpResponseRedirect("/")
        else:
            return super(PasswordResetFromKeyView, self).dispatch(request, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            try:
                return form.save(request, self.account_id, self.key, self.get_success_url())
            except Exception as e:
                form.add_error(NON_FIELD_ERRORS, e)

        return self.form_invalid(form)


def check_account_group(user_login, group):
    is_merchandise = False
    group_permissions = user_login['group_permissions']
    for key, value in group_permissions.iteritems():
        if str(key) == group:
            is_merchandise = True

    return is_merchandise


class DashboardPlayerView(AuthenticatedView, TemplateView):
    app_name = APP_NAME
    template_name = "dashboard"

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)

        url = API_URL + "/api/v1/account/info"
        headers = {'Authorization': str(self.user_login['account_id']) + ":" + str(self.user_login['api_key'])}

        r = requests.get(url, headers=headers)
        status_code = r.status_code
        response = r.json()

        if status_code == 200:
            if response['success']:
                context['sakura_coin'] = response['sakuraCoin']
                context['lock_reason'] = response['lockReason']
                context['lock_started_at'] = response['lockStartedAt']
                context['lock_ended_at'] = response['lockEndedAt']
                context['account_status'] = response['accountStatus']
                context['char_info_list'] = response['charInfoList']
            else:
                context['error_message'] = unicode(response['errorMessage'])
        else:
            logger.exception("Failed to get account info. Exception:" + response['error'])
            context['error_message'] = u"Có lỗi xảy ra. Không thể tìm thấy thông tin tài khoản"

        context['is_merchandise'] = check_account_group(self.user_login, MERCHANDISE)
        context['is_admin'] = check_account_group(self.user_login, ADMIN)
        return self.render_to_response(context)


class DashboardMerchantView(AuthenticatedView, AppFormView):
    app_name = APP_NAME
    template_name = "dashboard_merchant"
    form_class = RequestPaymentCodeForm
    success_url = "/account/dashboard/merchant?message_type=action_success"

    def get(self, request, *args, **kwargs):
        if not check_account_group(self.user_login, MERCHANDISE):
            return HttpResponseRedirect("/account/dashboard")

        context = self.get_context_data(**kwargs)

        url = API_URL + "/api/v1/payment/internal/code/list"
        headers = {'Authorization': str(self.user_login['account_id']) + ":" + str(self.user_login['api_key'])}

        r = requests.get(url, headers=headers)
        status_code = r.status_code
        response = r.json()

        print response

        if status_code == 200:
            if response['success']:
                context['payment_codes'] = response['paymentCodes']
            else:
                context['error_message'] = unicode(response['errorMessage'])
        else:
            logger.exception("Failed to get account info. Exception:" + response['error'])
            context['error_message'] = u"Có lỗi xảy ra. Không thể tìm thấy thông tin tài khoản"

        return self.render_to_response(context)


class DashboardAdminView(AuthenticatedView, TemplateView):
    app_name = APP_NAME
    template_name = "dashboard_admin"

    def get(self, request, *args, **kwargs):
        if not check_account_group(self.user_login, ADMIN):
            return HttpResponseRedirect("/account/dashboard")

        context = self.get_context_data(**kwargs)

        url = ADMIN_URL + "/admin/game/ccu"
        headers = {'Authorization': str(self.user_login['account_id']) + ":" + str(self.user_login['api_key'])}

        r = requests.get(url, headers=headers)
        status_code = r.status_code
        response = r.json()

        if status_code == 200:
            context['ccu'] = r.text
        else:
            logger.exception("Failed to get ccu info. Exception:" + response['error'])
            context['error_message'] = u"Có lỗi xảy ra. Không nhận được ccu"

        context['test_links'] = Config.objects.filter(key__contains="TEST_LINK")

        return self.render_to_response(context)
