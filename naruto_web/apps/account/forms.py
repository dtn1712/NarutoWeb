# coding=utf-8
import datetime
import logging

import requests
from django import forms
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _

from naruto_web.apps.app_helper import get_next_redirect_url, get_request_param, unix_time_millis, get_user_login_object
from naruto_web.settings import API_URL, WEBSITE_URL

logger = logging.getLogger(__name__)

PASSWORD_MIN_LENGTH = 6


def get_redirect_url(request, url=None, redirect_field_name="next"):
    if url and callable(url):
        # In order to be able to pass url getters around that depend
        # on e.g. the authenticated state.
        url = url()
    redirect_url = (url or get_next_redirect_url(request, redirect_field_name=redirect_field_name))
    return redirect_url


class SetPasswordField(forms.CharField):
    def clean(self, value):
        value = super(SetPasswordField, self).clean(value)
        min_length = PASSWORD_MIN_LENGTH
        if len(value) < min_length:
            raise forms.ValidationError(_(u'Password tối thiểu có ' + str(min_length) + u' ký tự.'))
        return value


class AccountForm(forms.Form):
    email = forms.EmailField(label=_("Email"), max_length=60,
                             widget=forms.TextInput(attrs={'placeholder': 'Email', 'class': "form-control"}))
    password = forms.CharField(label=_("Password"),
                               widget=forms.PasswordInput(attrs={'placeholder': "Password", "class": "form-control"}))

    def save(self, request, redirect_url=None):
        action_type = get_request_param(request, "action_type")
        data = {
            "username": self.cleaned_data["email"],
            "password": self.cleaned_data["password"]
        }

        url = API_URL + "/api/v1/account/login"
        if action_type == 'signup':
            url = API_URL + "/api/v1/account/signup"

        r = requests.post(url, json=data)
        status_code = r.status_code
        response = r.json()

        if status_code == 200:
            if response['success']:
                user = {
                    "account_id": response['accountId'],
                    "api_key": response['apiKey'],
                    "group_permissions": response['groupPermissions'],
                    "list_char_info": response['charInfoList']
                }

                time_diff = unix_time_millis(datetime.datetime.fromtimestamp(long(response['expiredAt']) / 1000.0)) - \
                            unix_time_millis(datetime.datetime.now())

                request.session['user'] = user
                request.session.set_expiry(time_diff)
                return HttpResponseRedirect(get_redirect_url(request, redirect_url))
            else:
                raise Exception(response['errorMessage'])
        else:
            logger.exception("Failed to login. Exception:" + response['error'])
            raise Exception("Có lỗi xảy ra. Vui lòng thử lại")


class ResetPasswordForm(forms.Form):
    email = forms.EmailField(label=_("Email"), max_length=60,
                             widget=forms.TextInput(attrs={'placeholder': 'Email', 'class': "form-control"}))

    def save(self):
        data = {
            "email": self.cleaned_data["email"],
            "resetPasswordUrl": WEBSITE_URL + "/account/password/reset/key/%s/"
        }

        url = API_URL + "/api/v1/account/password/reset/confirmation"

        r = requests.post(url, json=data)
        status_code = r.status_code
        response = r.json()

        if status_code == 200:
            if response['success']:
                return self.cleaned_data["email"]
            else:
                raise Exception(response['errorMessage'])
        else:
            logger.exception("Failed to change password. Exception:" + response['error'])
            raise Exception("Có lỗi xảy ra. Vui lòng thử lại")


class ResetPasswordKeyForm(forms.Form):
    password1 = SetPasswordField(label=_("New Password"),
                                 widget=forms.PasswordInput(attrs={'placeholder': u'Mật khẩu mới', "class": "form-control"}))
    password2 = forms.CharField(label=_("Confirm Password"), widget=forms.PasswordInput(
        attrs={'placeholder': u'Khẳng định mật khẩu mới', "class": "form-control"}))

    def clean_password2(self):
        if "password1" in self.cleaned_data and "password2" in self.cleaned_data:
            if (self.cleaned_data["password1"]
                    != self.cleaned_data["password2"]):
                raise forms.ValidationError(_("Xin khẳng định mật khẩu mới thật chính xác"))
        return self.cleaned_data["password2"]

    def save(self, request, account_id, key, redirect_url):
        data = {
            "accountId": account_id,
            "confirmationCode": key,
            "newPassword": self.cleaned_data['password1']
        }

        url = API_URL + "/api/v1/account/password/reset/"

        r = requests.post(url, json=data)
        status_code = r.status_code
        response = r.json()

        print response

        if status_code == 200:
            if response['success']:
                request.session['message_data'] = {"message": "Bạn đã khôi phục mật khẩu thành công"}
                return HttpResponseRedirect(get_redirect_url(request, redirect_url))
            else:
                raise Exception(response['errorMessage'])
        else:
            logger.exception("Failed to reset password. Exception:" + response['error'])
            raise Exception("Có lỗi xảy ra. Vui lòng thử lại")


class ChangePasswordForm(forms.Form):
    old_password = forms.CharField(label=_("Current Password"), widget=forms.PasswordInput(
        attrs={'placeholder': u'Mật khẩu cũ', "class": "form-control"}))
    password1 = SetPasswordField(label=_("New Password"),
                                 widget=forms.PasswordInput(
                                     attrs={'placeholder': u'Mật khẩu mới', "class": "form-control"}))
    password2 = forms.CharField(label=_("Confirm New Password"), widget=forms.PasswordInput(
        attrs={'placeholder': u'Khẳng định mật khẩu mới', "class": "form-control"}))

    def clean_password2(self):
        if "password1" in self.cleaned_data and "password2" in self.cleaned_data:
            if (self.cleaned_data["password1"]
                    != self.cleaned_data["password2"]):
                raise forms.ValidationError(_("Xin khẳng định mật khẩu mới thật chính xác"))
        return self.cleaned_data["password2"]

    def save(self, request, redirect_url=None):
        user_login = get_user_login_object(request)
        data = {
            "oldPassword": self.cleaned_data["old_password"],
            "newPassword": self.cleaned_data["password1"]
        }

        headers = {'Authorization': str(user_login['account_id']) + ":" + str(user_login['api_key'])}
        url = API_URL + "/api/v1/account/password/change"

        r = requests.post(url, json=data, headers=headers)
        status_code = r.status_code
        response = r.json()

        if status_code == 200:
            if response['success']:
                request.session['message_data'] = {"message": "Bạn đã đổi mật khẩu thành công"}
                return HttpResponseRedirect(get_redirect_url(request, redirect_url))
            else:
                raise Exception(response['errorMessage'])
        else:
            logger.exception("Failed to change password. Exception:" + response['error'])
            raise Exception("Có lỗi xảy ra. Vui lòng thử lại")


class RequestPaymentCodeForm(forms.Form):
    amount = forms.IntegerField(widget=forms.TextInput(
        attrs={'placeholder': 'Coin', 'class': "form-control"}))

    def save(self, request, redirect_url=None):
        user_login = get_user_login_object(request)
        data = {
            "amount": self.cleaned_data["amount"],
        }

        headers = {'Authorization': str(user_login['account_id']) + ":" + str(user_login['api_key'])}
        url = API_URL + "/api/v1/payment/internal/code/request"

        r = requests.post(url, json=data, headers=headers)
        status_code = r.status_code
        response = r.json()

        print response

        if status_code == 200:
            if response['success']:
                request.session['message_data'] = {"message": "Bạn đã yêu cầu tạo mã thanh toán thành công. Hãy dùng mã thanh toán có hiệu lực ở bên dưới để nạp sakura coin trong game"}
                return HttpResponseRedirect(get_redirect_url(request, redirect_url))
            else:
                raise Exception(response['errorMessage'])
        else:
            logger.exception("Failed to change password. Exception:" + response['error'])
            raise Exception("Có lỗi xảy ra. Vui lòng thử lại")