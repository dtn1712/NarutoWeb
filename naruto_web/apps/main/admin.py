from django.contrib import admin

from naruto_web.apps.main.models import *


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ['user']


class PhotoAdmin(admin.ModelAdmin):
    list_display = ['unique_id', 'name', 'width', 'height', 'image_url', 'image_secure_url']


class ConfigAdmin(admin.ModelAdmin):
    list_display = ['unique_id', 'key',  'value']


admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Photo, PhotoAdmin)
admin.site.register(Config, ConfigAdmin)
