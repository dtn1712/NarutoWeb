from naruto_web.apps.app_helper import get_client_ip
from naruto_web.apps.app_settings import SITE_DATA
from naruto_web.settings import STAGE


def site_data(request):
    return SITE_DATA


def global_data(request):
    results = {}
    client_ip = get_client_ip(request)
    results['client_ip'] = client_ip
    results['stage'] = STAGE
    return results



