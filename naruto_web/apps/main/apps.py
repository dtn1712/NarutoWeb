import logging

from django.apps import AppConfig

logger = logging.getLogger(__name__)


class MainConfig(AppConfig):
    name = 'naruto_web.apps.main'

    def ready(self):
        from naruto_web.apps.main import signals
