import logging

import cloudinary
from django.conf import settings
from django.contrib.auth.models import User
from django.db import models

from naruto_web.apps.app_helper import generate_unique_id

logger = logging.getLogger(__name__)


def get_unique_id():
    return generate_unique_id()


class Photo(models.Model):
    unique_id = models.CharField(primary_key=True, max_length=100, unique=True, default=get_unique_id)
    name = models.CharField(max_length=150)
    image = models.ImageField(upload_to=settings.UPLOAD_FOLDER + "/photo/%Y/%m/%d")
    image_url = models.CharField(max_length=300, blank=True)
    image_secure_url = models.CharField(max_length=300, blank=True)
    rotation_angle = models.IntegerField(default=0)
    width = models.IntegerField(blank=True, null=True)
    height = models.IntegerField(blank=True, null=True)
    is_upload_success = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.OneToOneField(User, blank=True, null=True, related_name="photo_created_by")
    last_modified_at = models.DateTimeField(blank=True, null=True)
    last_modified_by = models.OneToOneField(User, blank=True, null=True, related_name='photo_last_modified_by')

    def __unicode__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        upload_result = cloudinary.uploader.upload(self.image, public_id=self.unique_id, width=self.width,
                                                   height=self.height, format="jpg", quality=70,
                                                   angle=self.rotation_angle)
        print self.unique_id
        self.image_url = upload_result['url']
        self.image_secure_url = upload_result['secure_url']
        self.is_upload_success = True
        super(Photo, self).save(force_insert, force_update, using, update_fields)


class File(models.Model):
    unique_id = models.CharField(primary_key=True, max_length=100, unique=True, default=get_unique_id)
    name = models.CharField(max_length=150)
    file = models.FileField(upload_to=settings.UPLOAD_FOLDER + "/file/%Y/%m/%d")
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.OneToOneField(User, blank=True, null=True, related_name="file_created_by")
    last_modified_at = models.DateTimeField(blank=True, null=True)
    last_modified_by = models.OneToOneField(User, blank=True, null=True, related_name='file_last_modified_by')


class Config(models.Model):
    unique_id = models.CharField(primary_key=True, max_length=100, unique=True, default=get_unique_id)
    key = models.CharField(max_length=100)
    value = models.CharField(max_length=300)
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.OneToOneField(User, blank=True, null=True, related_name="config_created_by")
    last_modified_at = models.DateTimeField(blank=True, null=True)
    last_modified_by = models.OneToOneField(User, blank=True, null=True, related_name='config_last_modified_by')


class UserProfile(models.Model):
    unique_id = models.CharField(primary_key=True, max_length=100, unique=True, default=get_unique_id)
    user = models.OneToOneField(User)



