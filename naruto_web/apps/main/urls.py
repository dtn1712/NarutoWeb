from django.conf.urls import *

from naruto_web.apps.main.views import MainView

urlpatterns = [
    url(r"^$", MainView.as_view(), name='home'),
]
