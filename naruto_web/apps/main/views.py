import logging

from django.http import HttpResponse
from django.http import JsonResponse
from django.template import engines
from django.utils import timezone
from django.views.generic import TemplateView

from naruto_web.apps.app_helper import get_base_template_path
from naruto_web.apps.app_views import AppBaseView
from naruto_web.settings import SERVER_IP

logger = logging.getLogger(__name__)

APP_NAME = "main"


class MainView(AppBaseView, TemplateView):
    app_name = APP_NAME
    template_name = "index"


def get_server_ip(request):
    return JsonResponse(SERVER_IP, safe=False)


def handler404(request):
    data = {}
    jinja_engine = engines['jinja']
    template = jinja_engine.get_template('sites/404.jinja.html')
    data['base_template'] = get_base_template_path(None, None)
    return HttpResponse(template.render(context=data, request=request))


def handler500(request):
    data = {}
    jinja_engine = engines['jinja']
    template = jinja_engine.get_template('sites/500.jinja.html')
    data['base_template'] = get_base_template_path(None, None)
    return HttpResponse(template.render(context=data, request=request))
