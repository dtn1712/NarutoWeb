# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-05-03 15:16
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import naruto_web.apps.main.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='File',
            fields=[
                ('unique_id', models.CharField(default=naruto_web.apps.main.models.get_unique_id, max_length=100, primary_key=True, serialize=False, unique=True)),
                ('name', models.CharField(max_length=150)),
                ('file', models.FileField(upload_to=b'upload/file/%Y/%m/%d')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('last_modified_at', models.DateTimeField(blank=True, null=True)),
                ('created_by', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='file_created_by', to=settings.AUTH_USER_MODEL)),
                ('last_modified_by', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='file_last_modified_by', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('unique_id', models.CharField(default=naruto_web.apps.main.models.get_unique_id, max_length=100, primary_key=True, serialize=False, unique=True)),
                ('name', models.CharField(max_length=150)),
                ('image', models.ImageField(upload_to=b'upload/photo/%Y/%m/%d')),
                ('image_url', models.CharField(blank=True, max_length=300)),
                ('image_secure_url', models.CharField(blank=True, max_length=300)),
                ('rotation_angle', models.IntegerField(default=0)),
                ('width', models.IntegerField(blank=True, null=True)),
                ('height', models.IntegerField(blank=True, null=True)),
                ('is_upload_success', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('last_modified_at', models.DateTimeField(blank=True, null=True)),
                ('created_by', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='photo_created_by', to=settings.AUTH_USER_MODEL)),
                ('last_modified_by', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='photo_last_modified_by', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('unique_id', models.CharField(default=naruto_web.apps.main.models.get_unique_id, max_length=100, primary_key=True, serialize=False, unique=True)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
