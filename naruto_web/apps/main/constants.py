
CACHE_KEY_SPECIAL_SEPARATOR = ":::"

EMPTY_STRING = ""

BLANK_CHOICE = ("", "------------")
NULL_CHOICE = (None, "")

VALID_FILE_SIZE = 5242880

DEFAULT_PAGE_SIZE = 25
PAGE_PARAMETER = "offset"
PAGE_SIZE_PARAMETER = "limit"

ACTIVATE_LINK = "activate_link"

ALL_TYPE = "all"

EMAIL_SEPARATOR = ";"

COMMON_SEPARATOR = EMAIL_SEPARATOR

PUBLIC = "1"
PRIVATE = "0"

UNLIMITED = -1

NEW = "1"
OLD = "0"

TRUE = "1"
FALSE = "0"

HIDE = "0"
SHOW = "1"

ACCEPT = "1"
PENDING = "0"
DECLINE = "-1"

APPROVED = "approved"
REJECTED = "rejected"

YES_VALUE = "1"
NO_VALUE = "0"

MONTH_SHORT_MAP = {
    1: "Jan",
    2: "Feb",
    3: "Mar",
    4: "Apr",
    5: "May",
    6: "Jun",
    7: "Jul",
    8: "Aug",
    9: "Sep",
    10: "Oct",
    11: "Nov",
    12: "Dec"
}

MONTH_FULL_MAP = {
    1: "January",
    2: "February",
    3: "March",
    4: "April",
    5: "May",
    6: "June",
    7: "July",
    8: "August",
    9: "September",
    10: "October",
    11: "November",
    12: "December"
}

DAY_IN_MONTH_MAP = {
    1: 31,
    2: 28,
    3: 31,
    4: 30,
    5: 31,
    6: 30,
    7: 31,
    8: 31,
    9: 30,
    10: 31,
    11: 30,
    12: 31
}

WEEK_DAY_TEXT_MAP = {
    "Mon": "Monday",
    "Tue": "Tuesday",
    "Wed": "Wednesday",
    "Thu": "Thursday",
    "Fri": "Friday",
    "Sat": "Saturday",
    "Sun": "Sunday"
}

WEEK_DAY_NUM_MAP = {
    0: "Monday",
    1: "Tuesday",
    2: "Wednesday",
    3: "Thursday",
    4: "Friday",
    5: "Saturday",
    6: "Sunday"
}

WEEK_DAY = (
    ('0', "Monday"),
    ('1', "Tuesday"),
    ('2', "Wednesday"),
    ('3', "Thursday"),
    ('4', "Friday"),
    ('5', "Saturday"),
    ('6', "Sunday")
)


YES_NO = (
    ("1", "Yes"),
    ("0", "No"),
)

YES_NO_WITH_BLANK_CHOICE = (
    BLANK_CHOICE,
    ("1", "Yes"),
    ("0", "No"),
)


CURRENCY_ICON_MAP = {
    "USD": "$"
}


