import logging

import jinja2
from django_jinja import library
from django import template

from naruto_web.apps.app_helper import read_catalogue
from naruto_web.settings import BUILD_VERSION_ID, DEFAULT_APP_NAME
from naruto_web.settings import PROJECT_PATH, STATIC_URL, PROJECT_NAME

logger = logging.getLogger(__name__)

register = template.Library()


@library.global_function
@jinja2.contextfunction
@register.simple_tag(takes_context=True)
def load_js(context):
    stage = context['stage']
    app_name = DEFAULT_APP_NAME if "app_name" not in context else context['app_name']
    # responsive_type = "non_responsive" if context['flavour'] == 'full' else "responsive"
    result = ''
    if stage == "dev":
        plugin_files, global_files, app_files = [], [], []
        read_catalogue(plugin_files, PROJECT_PATH + "/assets/static/js/plugins/", None)
        read_catalogue(global_files, PROJECT_PATH + "/assets/static/js/global/", None)
        read_catalogue(app_files, PROJECT_PATH + "/assets/static/js/apps/" + app_name + "/", None)
        for filename in plugin_files:
            result = result + '<script type="text/javascript" src="' + STATIC_URL + 'js/plugins/' + filename + '"></script>\n'

        for filename in global_files:
            result = result + '<script type="text/javascript" src="' + STATIC_URL + 'js/global/' + filename + '"></script>\n'

        for filename in app_files:
            result = result + '<script type="text/javascript" src="' + STATIC_URL + 'js/apps/' + app_name + '/' + filename + '"></script>\n'
    else:
        result = result + '<script type="text/javascript" src="' + STATIC_URL + 'js/prod/' + PROJECT_NAME + ".script." + app_name + "." + BUILD_VERSION_ID + '.min.js"></script>'

    return result


@library.global_function
@jinja2.contextfunction
@register.simple_tag(takes_context=True)
def load_css(context):
    stage = context['stage']
    app_name = DEFAULT_APP_NAME if "app_name" not in context else context['app_name']
    # responsive_type = "non_responsive" if context['flavour'] == 'full' else "responsive"
    result = ""
    if stage == "dev":
        # catalogue_type = NON_RESPONSIVE_TYPE if context['flavour'] == 'full' else  RESPONSIVE_TYPE
        css_path = PROJECT_PATH + "/assets/static/css/"
        list_file = []
        read_catalogue(list_file, css_path, None)
        for filename in list_file:
            result = result + '<link rel="stylesheet" href="' + STATIC_URL + 'css/' + filename + '" type="text/css" />\n'
        result = result + '<link rel="stylesheet" href="' + STATIC_URL + 'css/apps/' + app_name + '.css" type="text/css" />'
        return result
    else:
        return '<link rel="stylesheet" href="' + STATIC_URL + 'css/prod/stylesheets/' + app_name + '.' + BUILD_VERSION_ID + '.min.css" type="text/css" />'
