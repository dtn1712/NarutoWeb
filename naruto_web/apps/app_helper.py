import collections
import datetime
import decimal
import json
import logging
import os
import random
import requests
import shortuuid

from sys import path

from django.contrib.auth.models import User
from django.template import RequestContext, engines
from django.template.loader import render_to_string

from naruto_web.apps.app_settings import HTML_SNIPPET_TEMPLATE
from naruto_web.apps.app_settings import MESSAGE_SNIPPET_TEMPLATE
from naruto_web.apps.app_settings import SECONDS_PER_DAY, SECONDS_PER_HOUR, SECONDS_PER_MINUTE
from naruto_web.settings import PROJECT_NAME, SECRET_KEY, OW_LY_API_KEY, ROOT_PATH, DEFAULT_TEMPLATE_FILE_EXTENSION

logger = logging.getLogger(__name__)

CATALOGUE = "CATALOGUE"
COMMENT_CHARACTER = "<!--"

REDIRECT_FIELD_NAME = 'next'


def read_catalogue(list_file, filepath, filename):
    catalogue_path = filepath
    catalogue_file = CATALOGUE
    if filename is not None:
        catalogue_file = filename
    if catalogue_file not in filepath:
        catalogue_path = filepath + catalogue_file if filepath[
                                                          len(filepath) - 1] == "/" else filepath + "/" + catalogue_file

    if os.path.exists(catalogue_path):
        f = open(catalogue_path, "r")
        for filename in f:
            if len(filename.replace("\n", "")) != 0 and filename.startswith(COMMENT_CHARACTER) is False:
                list_file.append(filename.replace("\n", "").strip())
    else:
        return []


def merge_two_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z

def convert_list_to_json(data):
    d = []
    for i in range(0, len(data)):
        d.append(data[i]['fields'])
    return json.dumps(d)


def data_backup():
    path.append(ROOT_PATH)
    try:
        cmd = "python manage.py dumpdata > " + PROJECT_NAME + "/db/fixtures/"
        os.system(cmd)
    except Exception as e:
        logger.exception(e)


def make_two_numbers(original):
    if len(original) == 1:
        return "0" + original
    else:
        return original


def capitalize_first_letter(s):
    if s == None: return
    if len(s) == 0: return
    return s[0].upper() + s[1:].lower()


def json_encode_decimal(obj):
    if isinstance(obj, decimal.Decimal):
        return str(obj)
    raise TypeError(repr(obj) + " is not JSON serializable")


def convert_24hr_to_ampm(time):
    hour = time.hour
    minute = time.minute
    if int(hour) >= 12:
        final_hour = int(hour) + 1 - 12
        return str("%02d" % final_hour) + ":" + str("%02d" % minute) + "pm"
    else:
        return str("%02d" % hour) + ":" + str("%02d" % minute) + "am"


def convert_ampm_to_24hr(time, ampm):
    hour = time.hour
    result = ""
    if ampm.lower() == 'am':
        if int(hour) == 12:
            result = "00:"
        else:
            result = make_two_numbers(hour) + ":"
    if ampm.lower() == 'pm':
        if int(hour) == 12:
            result = "12:"
        else:
            result = str(12 + hour) + ":"
    return result + str(time.minute) + " " + ampm


def get_elapse_time_text(value):
    # Get total elapse seconds
    elapse_time = datetime.datetime.utcnow() - datetime.datetime(value.year, value.month, value.day, value.hour,
                                                                 value.minute, value.second)
    total_seconds = elapse_time.total_seconds()

    days = int(total_seconds / SECONDS_PER_DAY)
    hours = int((total_seconds % SECONDS_PER_DAY) / SECONDS_PER_HOUR)
    minutes = int(((total_seconds % SECONDS_PER_DAY) % SECONDS_PER_HOUR) / SECONDS_PER_MINUTE)

    if days == 0 and hours == 0 and minutes == 0:
        return "Just now"

    min_value, hour_value, day_value = ' minute', ' hour', ' day'
    if minutes != 1: min_value += "s"
    if hours != 1: hour_value += "s"
    if days != 1: day_value += "s"

    if days == 0 and hours == 0:
        return str(minutes) + min_value + " ago"
    if days == 0:
        return str(hours) + hour_value + " ago"
    return str(days) + day_value + " ago"


def is_empty(s):
    if s == None: return True
    if len(s.strip()) == 0: return True
    return False


def get_duplicate_object(l):
    l2 = collections.Counter(l)
    return [i for i in l2 if l2[i] > 1]


def remove_duplicate_object(l):
    return list(set(l))


def set_fixed_string(s, s_len):
    if s_len >= len(s):
        return s
    return s[:s_len] + '...'


# Get current login user object
def get_user_login_object(request):
    return request.session.get("user", None)


# Get the current ip from client to see their zip code and
# return appropriate location. Currently, this is not work with localhost
def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        return x_forwarded_for.split(',')[-1].strip()
    else:
        return request.META.get('REMOTE_ADDR')


# Setup the constant month tuple using for the form
def setup_constant_month():
    month_value = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    l = []
    for i in range(1, 13):
        l.append([i, month_value[i - 1]])
    return tuple(tuple(x) for x in l)


# Setup the constant day tuple
def setup_constant_day():
    l = []
    for i in range(1, 32):
        l.append([i, i])
    return tuple(tuple(x) for x in l)


# Setup the constant year tuple
def setup_constant_year(start_year=1920, is_string=False):
    l = []
    current_year = datetime.datetime.now().year
    for i in range(start_year, int(current_year) - 13):
        if is_string:
            l.append([str(i), str(i)])
        else:
            l.append([i, i])
    return tuple(tuple(x) for x in l)


def generate_html_snippet(request, snippet, data, template_engine='jinja'):
    template_name = HTML_SNIPPET_TEMPLATE[snippet]
    if template_engine == "jinja":
        jinja_engine = engines['jinja']
        template = jinja_engine.get_template(template_name)
        return template.render(context=data, request=request)
    else:
        return render_to_string(template_name, data, context_instance=RequestContext(request))


def generate_message(template_name, data):
    try:
        message = render_to_string(MESSAGE_SNIPPET_TEMPLATE[template_name], data)
        return message
    except Exception as e:
        logger.exception(e)
    return None


def handle_request_get_message(request, data):
    if "message_type" in request.GET:
        return generate_message(request.GET['message_type'], merge_two_dicts(data, request.session.get("message_data", {})))
    return None


def get_any_admin_object():
    admins = User.objects.filter(is_superuser=True)
    if len(admins) == 0:
        return None
    else:
        return admins[0]


def get_short_url(request):
    long_url = str(request.build_absolute_uri())
    try:
        r = requests.get("http://ow.ly/api/1.1/url/shorten?apiKey=" + OW_LY_API_KEY + "&longUrl=" + long_url)
        data = r.json()
        return data['results']['shortUrl']
    except:
        pass
    return long_url


def generate_unique_id():
    return shortuuid.uuid()[:11] + shortuuid.uuid()[:11] + str("%03d" % random.randint(1, 999))


def get_base_template_path(app_name=None, template_extension=None):
    if app_name is None:
        if template_extension is None:
            return "sites/base" + DEFAULT_TEMPLATE_FILE_EXTENSION
        else:
            return "sites/base" + template_extension
    else:
        if template_extension is None:
            return "sites/apps/" + app_name + "/" + app_name + "_base" + DEFAULT_TEMPLATE_FILE_EXTENSION
        else:
            return "sites/apps/" + app_name + "/" + app_name + "_base" + template_extension


def get_template_path(app_name, template_name, sub_path='/page/', template_extension=None):
    if template_extension is None:
        return "sites/apps/" + app_name + sub_path + template_name + DEFAULT_TEMPLATE_FILE_EXTENSION
    else:
        return "sites/apps/" + app_name + sub_path + template_name + template_extension


def check_key_not_blank(data, key):
    if key in data and data[key] is not None and len(data[key]) != 0:
        return True
    return False


def set_null_if_empty(s):
    if s is None:
        return s

    if len(s.strip()) > 0:
        return s
    else:
        return None


def convert_bool_to_binary(value):
    if value is None:
        return None
    if value:
        return "1"
    return "0"


def get_request_param(request, param, default=None):
    return request.POST.get(param) or request.GET.get(param, default)


def get_next_redirect_url(request, redirect_field_name="next"):
    """
    Returns the next URL to redirect to, if it was explicitly passed
    via the request.
    """
    redirect_to = get_request_param(request, redirect_field_name)
    # if not get_adapter().is_safe_url(redirect_to):
    #     redirect_to = None
    return redirect_to


def unix_time_millis(dt):
    return (dt - datetime.datetime.utcfromtimestamp(0)).total_seconds() * 1000.0

