var is_login = $("#is_user_login").val();
var user_login_username = (is_login) ? $("#user_login_username").val() : null;
var loading = $.loading();

function showMessage(data,el) {
    var new_item = $(data).hide();
    $(el).append(new_item);
    new_item.slideDown();
    setTimeout(function(){
        new_item.slideUp(function(){
            jQuery(this).remove();
        });
    },3000);
}

// Another function of getting cursor
function getCaret(el) {
  if (el.selectionStart) {
    return el.selectionStart;
  } else if (document.selection) {
    el.focus();

    var r = document.selection.createRange();
    if (r == null) {
      return 0;
    }

    var re = el.createTextRange(),
        rc = re.duplicate();
    re.moveToBookmark(r.getBookmark());
    rc.setEndPoint('EndToStart', re);

    return rc.text.length;
  }
  return 0;
}

function stripLargeInnerWhitespace(str) {
    return str.replace( /  +/g, ' ' )
}

function stripLargeOuterWhitespace(str) {
   return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '')
}

// Strip whitespace function
function stripWhitespace(str) {
    return stripLargeInnerWhitespace(stripLargeOuterWhitespace(str));
}

// Convert 24hr system to am/pm system
function convert24hrToAmPm(year,month,day,hour,minute){
    var minute_str = minute.toString();
    if (minute < 10){
        minute_str = "0" + minute.toString();
    }
    if (hour == 0)
        return "12" + ":" + minute_str+ "am";
    if ((hour > 0) && (hour < 12))
        return hour.toString() + ":" + minute_str + "am";
    if (hour == 12)
        return "12" + ":" + minute_str + "pm";
    if (hour > 12)
        return (hour-12).toString() + ":" + minute_str + "pm";
}

// Simple function return this type of format: Wednesday, January 13, 2012
function convertDateName(year,month,day,weekday) {
  return week_day_num[weekday] + ", " + month_name_full[month] + " " + day + ", " + year;
}

function isNumber (o) {
  return !isNaN(parseFloat(o));
}

function isEmpty(el) {
    if(($(el).val().length==0) || ($(el).val()==null)) {
      return true;
    }
    return false
}

function isExist(el) {
    if ($(el).length > 0){
      return true
    }
    return false
}

function isUndefined(el) {
  if (typeof el === "undefined") return true;
  return false;
}

function isVisible(el) {
  if ($(el).hasClass("hide")) return false;
  return true;
}

function isValidEmail(value) {
  var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
  return re.test(value);
}

// Return a datetime format for comment: June 19, 2012, 12:42pm. All the function here can be found in common.js
function standardDatetimeFormat(year,month,day,hour,minute) {
    return month_name_full[month] + " " + day + ", " + year + ", " + convert_24hr_to_AM_PM(year,month,day,hour,minute)['string']
}

function scrollPageTop() {
  $("html, body").animate({scrollTop:0}, '300', 'swing');
}

function scrollPagePos(pos) {
   $("html, body").animate({scrollTop:pos}, '300', 'swing');
}

function stretchWindowMinHeight(el,offset) {
  if(typeof(offset)==='undefined') offset = 0;
  $(el).css("min-height",$(window).height()-offset);
  $(window).resize(function() {
    $(el).css("min-height",$(window).height()-offset);
  })
}

function stretchToElementHeight(el1,el2) {
  $(el1).css("height",$(el2).height());
  $(window).resize(function() {
    $(el1).css("height",$(el2).height());
  })
}

function getMultipleCheckbox(el){
    var result = [];
    $(el).each(function(index, value){
        var name =  $(this).attr("name");
        result.push(parseInt(name.substring(7,name.length),10));
    });
    return result
}

function capitalizeFirstLetter(string){
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function setNullForUndefined(variable) {
  if (typeof variable === 'undefined') variable = null;
  return variable;
}

function setEmptyForUndefined(variable) {
  if (typeof variable === 'undefined') variable = "";
  return variable;
}

function setDefaultValueForUndefined(variable,value) {
  if (typeof variable === 'undefined') variable = value;
  return variable;
}

function countOccurrences(str, value){
   var regExp = new RegExp(value, "gi");
   return str.match(regExp) ? str.match(regExp).length : 0;
}

function appendNewItem(el,data) {
    var new_item = $(data).hide();
    $(el).append(new_item);
    new_item.slideDown();
}

function prependNewItem(el,data) {
    var new_item = $(data).hide();
    $(el).prepend(new_item);
    new_item.slideDown();
}

function showAjaxLoadingIcon() {
    loading.open();
}

function hideAjaxLoadingIcon() {
   loading.close();
}

function addDotToLongTextStringOutput(el,total_char_num) {
    var content = stripWhitespace(el);
    if (content.length > total_char_num) {
      var new_value = content.substring(0,total_char_num) + "...";
      return new_value;
    }
    return content;
}

function addDotToLongTextWithNumChar(el,total_char_num) {
    $(el).each(function() {
        var content = stripWhitespace($(this).text())
        if (content.length > total_char_num) {
            var last_pos = content.substring(0,total_char_num).lastIndexOf(" ");
            var new_value = content.substring(0,last_pos) + "...";
            $(this).html(new_value);
        }
    })
}

function addDotToLongTextWithElementAttr(el,attr_value) {
    $(el).each(function() {
        var content = stripWhitespace($(this).text())
        var total_char_num = $(this).attr(attr_value);
        if (content.length > total_char_num) {
            var last_pos = content.substring(0,total_char_num).lastIndexOf(" ");
            var new_value = content.substring(0,last_pos) + "...";
            $(this).html(new_value);
        }
    })
}

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)  {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
    return null;
}

jQuery.fn.outerHTML = function() {
    return jQuery('<div />').append(this.eq(0).clone()).html();
}
