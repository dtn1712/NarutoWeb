function listFilter(items,query) {
    var rg = new RegExp(query,'i');
    $(items).each(function(){
        if($.trim($(this).text()).search(rg) == -1) {
            $(this).closest("li").css('display', 'none');
        } else {
            $(this).closest('li').css('display', '');
        }
    });
}

function showAlertMessage(message,alert_type,is_time_out) {
    if ($("#alert_message").is(":visible")){
        $("#alert_message").slideUp();
    }
    $("#alert_message").removeClass();
    $("#alert_message").attr("role","alert");
    if (alert_type) {
        $("#alert_message").attr("class","alert alert-" + alert_type)
    } else {
        $("#alert_message").attr("class","alert alert-normal alert-dismissible")
    }
    $("#alert_message .message-content").html(message);
    $("#alert_message").slideDown();

    if (is_time_out) {
        setTimeout(function() {
            $("#alert_message").slideUp('slow');
        },8000)
    }
}

function hideAlertMessage() {
    $("#alert_message").slideUp();
}


function htmlUtf8Decode(input){
    var e = document.createElement('div');
    e.innerHTML = input;
    return e.childNodes[0].nodeValue;
}

function runTopProgressBar() {
    NProgress.set(Math.random());
    NProgress.start();
    NProgress.inc();
    setTimeout(function(){
        NProgress.done();
    }, 15000);
}

function checkLogin(redirect_link) {
    if (is_login == "False") {
        window.location.href = WEBSITE_URL + "/accounts/login/?next=" + redirect_link;
    }
}

function isTriggerLoadingScroll() {
    if ($(window).scrollTop() == $(document).height() - $(window).height()) {
        return true;
    }
    return false;
}

function clearModalError(modal_el) {
    var error_el = $(modal_el).find(".error");
    for (var i = 0; i < error_el.length; i++) {
        var el = error_el[i];
        $(el).removeClass("error");
    }
}

function getPageType() {
    return $("#page_type").val();
}


// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});