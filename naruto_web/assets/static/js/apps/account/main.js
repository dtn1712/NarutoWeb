var page_type = getPageType();

var next_parameter = getUrlParameter("next");

function submitForm(action_type) {
    $("form input[name='action_type']").val(action_type);
    return true;
}

$(function() {

	if (page_type == 'account_login') {
		if (next_parameter != null) {
			$("form#login_form input[name='next']").val(next_parameter);
		}
	}
})


$(function() {
    $("#reload_game_btn").click(function() {
        showAjaxLoadingIcon();
        $.ajax({
            url: '/ajax/admin/reload_game',
            type: 'POST',
            success: function (data) {
                if (data['success']) {
                    showAlertMessage("Bạn đã reload game data thành công", "success", true)
                } else {
                    showAlertMessage(data['error_message'], "danger", true)
                }
            }
        });
    })
})