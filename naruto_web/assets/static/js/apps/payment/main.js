$(function() {
    $("#payment_method_select").on('change', function() {
        var value = this.value;
        if (value.length == 0) {
            $("#amount_content").html('');
            $("#select_amount_section").addClass("hidden");
            $(".payment-section").addClass("hidden");
            $("#make_payment_btn").attr("disabled", "disabled");
            return;
        }

        $("#make_payment_btn").removeAttr("disabled");
        var typeKey = value.split("-")[0];
        var group = value.split("-")[1];
        showAjaxLoadingIcon();
        $.ajax({
            url: '/ajax/payment/exchange_rate?payment_method=' + typeKey,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                if (data['success']) {
                    $("#amount_content").html(data['message_snippet']);
                    $("#select_amount_section").removeClass("hidden");

                    $(".payment-section").addClass("hidden");
                    $("#" + group.toLowerCase() + "_section").removeClass("hidden");
                } else {
                    showAlertMessage(data['error_message'], "danger", true)
                }
            }
        });
    });
})