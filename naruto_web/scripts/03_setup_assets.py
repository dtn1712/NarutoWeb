import os
import shutil
import sys

BUILD_STAGE = ["prod", "beta"]

PROJECT_PATH = os.path.abspath(__file__ + "/../../")
PROJECT_NAME = "naruto_web"

CATALOGUE = "CATALOGUE"
COMMENT_CHARACTER = "<!--"

YUI_COMPRESSOR = PROJECT_NAME + "/libs/compressor/yuicompressor-2.4.8.jar"
HTML_COMPRESSOR = PROJECT_NAME + "/libs/compressor/htmlcompressor-1.5.3.jar"
COMPRESSOR = {"html": HTML_COMPRESSOR, "css": YUI_COMPRESSOR, "js": YUI_COMPRESSOR}

SETTING_PATH = os.path.abspath(__file__ + "/../../")
PROJECT_ROOT = os.path.abspath(__file__ + "/../../../")

sys.path.append(SETTING_PATH)
sys.path.append(PROJECT_ROOT)

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from naruto_web.settings import CSS_STATIC_URL_PATH_REPLACEMENT


def compress(in_files, out_file, in_type='js', verbose=False,
             temp_file='.temp'):
    temp = open(temp_file, 'w')
    for f in in_files:
        fh = open(f)
        data = fh.read() + '\n'
        fh.close()

        temp.write(data)

        print ' + %s' % f
    temp.close()

    options = ['-o "%s"' % out_file,
               '--type %s' % in_type]

    if in_type == "html":
        options.append("--compress-js")
        options.append("--compress-css")

    if verbose:
        options.append('-v')

    cmd = 'java -jar "%s" %s "%s"' % (COMPRESSOR[in_type], ' '.join(options), temp_file)
    os.system(cmd)

    org_size = os.path.getsize(temp_file)
    new_size = os.path.getsize(out_file)

    print '=> %s' % out_file
    print 'Original: %.2f kB' % (org_size / 1024.0)
    print 'Compressed: %.2f kB' % (new_size / 1024.0)
    print 'Reduction: %.1f%%' % (float(org_size - new_size) / org_size * 100)
    print ''


def list_files(root_dir, file_list):
    for root, subFolders, files in os.walk(root_dir):
        for file in files:
            f = os.path.join(root, file)
            file_list.append(f)


def copy_recursive(src, dest):
    if os.path.exists(dest):
        shutil.rmtree(dest)
    shutil.copytree(src, dest)


def read_catalogue(list_file, filepath, filename):
    catalogue_path = filepath
    catalogue_file = CATALOGUE
    if filename is not None:
        catalogue_file = filename
    if catalogue_file not in filepath:
        catalogue_path = filepath + catalogue_file if filepath[len(filepath) - 1] == "/" else filepath + "/" + catalogue_file

    if os.path.exists(catalogue_path):
        f = open(catalogue_path, "r")
        for filename in f:
            if len(filename.replace("\n", "")) != 0 and filename.startswith(COMMENT_CHARACTER) is False:
                list_file.append(filepath + "/" + filename.replace("\n", "").strip())
    else:
        return []


def get_or_create_folder(folder_path):
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
    return folder_path


def clean_folder():
    js_dest = get_or_create_folder(PROJECT_PATH + "/assets/static/js/prod/")

    for filename in os.listdir(js_dest):
        if "js" in filename:
            os.remove(js_dest + filename)

    css_dest = get_or_create_folder(PROJECT_PATH + "/assets/static/css/prod/")

    css_cleanup_dest = css_dest + "stylesheets/"
    for filename in os.listdir(css_cleanup_dest):
        if "css" in filename:
            os.remove(css_cleanup_dest + filename)


def setup_static(build_version_id):
    get_or_create_folder(PROJECT_PATH + "/assets/static/js/prod/")

    plugin_files, global_files = [], []
    read_catalogue(plugin_files, PROJECT_PATH + "/assets/static/js/plugins/", None)
    read_catalogue(global_files, PROJECT_PATH + "/assets/static/js/global/", None)


    # # Minify the javascript of each app
    js_src = PROJECT_PATH + "/assets/static/js/apps/"
    files = os.listdir(js_src)
    for filename in files:
        dir_app = js_src + filename + "/"
        subapp_plugin_scripts = []
        if os.path.exists(dir_app + "plugins"):
            subapp_plugin_files = os.listdir(dir_app + "plugins")
            for plugin_file in subapp_plugin_files:
                subapp_plugin_scripts.append(dir_app + "plugins/" + plugin_file)

        app_scripts = []
        read_catalogue(app_scripts, PROJECT_PATH + "/assets/static/js/apps/" + filename, None)

        final_scripts = plugin_files + global_files + subapp_plugin_scripts + app_scripts
        final_scripts_out = PROJECT_PATH + "/assets/static/js/prod/" + PROJECT_NAME + ".script." + filename + "." + build_version_id + ".min.js"
        compress(final_scripts, final_scripts_out, 'js', False)

    css_dest = get_or_create_folder(PROJECT_PATH + "/assets/static/css/prod/")

    # Copy css resource
    css_resources_src = PROJECT_PATH + "/assets/static/css/resources/"
    resources_files = os.listdir(css_resources_src)
    for filename in resources_files:
        copy_recursive(css_resources_src + filename, css_dest + filename)

    # Minify the css file in catalogue
    css_common_src = PROJECT_PATH + "/assets/static/css/"
    style_css = []
    read_catalogue(style_css, css_common_src, None)

    style_css_out = css_dest + "stylesheets/styles.min.css"
    get_or_create_folder(css_dest + "stylesheets/")
    compress(style_css, style_css_out, 'css')

    # List all processing css file
    css_subapp_src = PROJECT_PATH + "/assets/static/css/apps/"
    subapp_files = os.listdir(css_subapp_src)

    # Inject absolute url path to data contain relative path point to resources
    get_or_create_folder(css_dest + "tmp/")
    final_subapp_files = []
    for filename in subapp_files:
        final_subapp_files.append(filename)

    copy_recursive(css_subapp_src,css_dest + "tmp/")
    # Minify the css for each sub app combine with the global stylesheet
    for filename in final_subapp_files:
        subapp_css = [style_css_out, css_dest + "tmp/" + filename]
        subapp_css_out_tmp = css_dest + "tmp/" + filename
        subapp_css_out = css_dest + "stylesheets/" + filename[0:filename.rfind(".")] + "." + build_version_id + ".min.css"
        compress(subapp_css, subapp_css_out_tmp, "css")

        f1 = open(subapp_css_out_tmp, "r")
        f2 = open(subapp_css_out, "w")
        content = f1.read()
        for key, value in CSS_STATIC_URL_PATH_REPLACEMENT.iteritems():
            content = content.replace(key, value)
        f2.write(content)
        f1.close()
        f2.close()

    # cleanup
    os.system("rm -rf " + css_dest + "tmp/")


def main():
    print "...RUNNING SETUP ASSETS SCRIPT..."
    try:
        stage = sys.argv[1]
        build_version_id = sys.argv[2]
        if stage in BUILD_STAGE:
            clean_folder()
            setup_static(build_version_id)
            print "Setup assests successfully"
        else:
            print "No need to compress asset in the development mode"
    except Exception:
        print "Setup assets error"
        raise


if __name__ == "__main__":
    main()
