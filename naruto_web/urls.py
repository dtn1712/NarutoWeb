from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

from naruto_web import settings
from naruto_web.apps.main.views import get_server_ip

admin.autodiscover()


urlpatterns = [
                  # Admin URL
                  url(r'^admin/', include(admin.site.urls)),
                  url(r'^admin/defender/', include('defender.urls')),  # defender admin

                  # Auth
                  url(r'^account/', include("naruto_web.apps.account.urls")),

                  url(r'^ajax/', include("naruto_web.apps.ajax.urls")),

                  url(r'^about/', include("naruto_web.apps.about.urls")),
                  url(r'^photo/', include('naruto_web.apps.photo.urls')),
                  url(r'^payment/', include('naruto_web.apps.payment.urls')),

                  url(r"^server_ip$", get_server_ip, name="server_ip"),
                  url(r'^$', include('naruto_web.apps.main.urls')),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler404 = "naruto_web.apps.main.views.handler404"
handler500 = "naruto_web.apps.main.views.handler500"
